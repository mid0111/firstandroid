package com.mid.firstandroid.model;

/**
 * Created by mid on 14/04/25.
 */
public class Message {
    private final Long created;
    private final String message;
    private final String userId;
    private final String id;

    public Message(String id, String userId, String message, Long created) {
        this.id = id;
        this.userId = userId;
        this.message = message;
        this.created = created;
    }

    public String getMessage() {
        return message;
    }
}
