package com.mid.firstandroid.model;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mid on 14/04/25.
 */
public class Messages extends AbstractList<Message> {

    List<Message> messages = new ArrayList<Message>();

    @Override
    public Message get(int location) {
        return this.messages.get(location);
    }

    @Override
    public int size() {
        return this.messages.size();
    }

    public boolean add(Message message) {
        return this.messages.add(message);
    }
}
