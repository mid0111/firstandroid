package com.mid.firstandroid.app;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import com.mid.firstandroid.accessor.MessageAccessor;
import com.mid.firstandroid.model.Message;
import com.mid.firstandroid.model.Messages;

import java.util.UUID;

public class MainActivity extends ActionBarActivity {

    private static final String BR = System.getProperty("line.separator");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setRegisteredMessage();
    }

    private void setRegisteredMessage() {
        // Collect registered message from db
        MessageAccessor accessor = new MessageAccessor(getApplicationContext());
        Messages messages = accessor.list();

        // Initialize message feed
        StringBuilder builder = new StringBuilder();
        for(Message message : messages) {
            builder.append(message.getMessage());
            builder.append(BR);
        }
        builder.deleteCharAt(builder.length() - 1);

        setMessageFeed(builder.toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        getMenuInflater().inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle press on the action bar items
        switch (item.getItemId()) {
            case R.id.action_search:
                openSearch();
                return true;

            case R.id.action_settings:
                openSettings();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setMessageFeed(String text) {
        // Set receive message
        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setTextSize(15);
        textView.setText(text);

        // Scroll to end of line
        ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView);
        scrollView.fullScroll(View.FOCUS_DOWN);
    }

    /** Called when the user clicks the Send button */
    public void sendMessage(View view) {
        // Get message from edit field
        EditText editText = (EditText) findViewById(R.id.edit_message);
        String message = editText.getText().toString();

        if(message.length() < 1) {
            // Do nothing When message is blank.
            return;
        }

        // Save message
        MessageAccessor accessor = new MessageAccessor(getApplicationContext());
        accessor.write(UUID.randomUUID().toString(), "", message);

        // Clear edit field
        editText.setText("");

        // Set receive message
        TextView textView = (TextView) findViewById(R.id.textView);
        String existsMessage = textView.getText().toString();
        setMessageFeed(existsMessage + BR + message);
    }

    private void openSettings() {

    }

    private void openSearch() {

    }

}
