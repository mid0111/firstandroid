package com.mid.firstandroid.accessor;

import android.provider.BaseColumns;

/**
 * Created by mid on 14/04/25.
 */
public class MessageContract {

    public MessageContract() {}

    public static abstract class MessageEntry implements BaseColumns {
        public static final String TABLE_NAME = "entry";
        public static final String COLUMN_NAME_ENTRY_ID = "id";
        public static final String COLUMN_NAME_USER_ID = "userId";
        public static final String COLUMN_NAME_MESSAGE = "message";
        public static final String COLUMN_NAME_CREATED = "created";
        public static final String COLUMN_NAME_NULLABLE = "null";
    }

    public static abstract class SQL {
        public static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE `" + MessageContract.MessageEntry.TABLE_NAME + "` " +
                "(" +
                    MessageContract.MessageEntry._ID + " INTEGER PRIMARY KEY," +
                    MessageEntry.COLUMN_NAME_ENTRY_ID + " TEXT," +
                    MessageEntry.COLUMN_NAME_USER_ID + " TEXT," +
                    MessageEntry.COLUMN_NAME_MESSAGE + " TEXT," +
                    MessageEntry.COLUMN_NAME_CREATED + " LONG" +
                " )";

        public static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS `" + MessageEntry.TABLE_NAME + "`";
    }

}
