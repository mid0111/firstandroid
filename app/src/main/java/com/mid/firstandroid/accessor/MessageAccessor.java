package com.mid.firstandroid.accessor;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.mid.firstandroid.model.Message;
import com.mid.firstandroid.model.Messages;

/**
 * Created by mid on 14/04/25.
 */
public class MessageAccessor {

    private MessageDbHelper mDbHelper = null;

    public MessageAccessor(Context context) {
        this.mDbHelper = new MessageDbHelper(context);
    }

    public void write(String id, String userId, String message) {
        SQLiteDatabase db = this.mDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(MessageContract.MessageEntry.COLUMN_NAME_ENTRY_ID, id);
        values.put(MessageContract.MessageEntry.COLUMN_NAME_USER_ID, userId);
        values.put(MessageContract.MessageEntry.COLUMN_NAME_MESSAGE, message);
        values.put(MessageContract.MessageEntry.COLUMN_NAME_CREATED, System.currentTimeMillis());

        long newRowId = db.insert(
                MessageContract.MessageEntry.TABLE_NAME,
                MessageContract.MessageEntry.COLUMN_NAME_NULLABLE,
                values
        );
    }

    public Messages list() {
        SQLiteDatabase db = this.mDbHelper.getReadableDatabase();

        String[] projection = {
                MessageContract.MessageEntry._ID,
                MessageContract.MessageEntry.COLUMN_NAME_ENTRY_ID,
                MessageContract.MessageEntry.COLUMN_NAME_USER_ID,
                MessageContract.MessageEntry.COLUMN_NAME_MESSAGE,
                MessageContract.MessageEntry.COLUMN_NAME_CREATED
        };

        String sortOrder = MessageContract.MessageEntry.COLUMN_NAME_CREATED + " DESC";

        String selection = null;
        String[] selectionArgs = null;
        String limit = "30";

        Cursor c = db.query(
                MessageContract.MessageEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder,
                limit
        );

        Messages messages = new Messages();
        int rowCount = c.getCount();
        c.moveToFirst();
        for(int i = 0 ; i < rowCount ; i++) {
            String id = c.getString(c.getColumnIndexOrThrow(MessageContract.MessageEntry.COLUMN_NAME_ENTRY_ID));
            String userId = c.getString(c.getColumnIndexOrThrow(MessageContract.MessageEntry.COLUMN_NAME_USER_ID));
            String message = c.getString(c.getColumnIndexOrThrow(MessageContract.MessageEntry.COLUMN_NAME_MESSAGE));
            Long created = c.getLong(c.getColumnIndexOrThrow(MessageContract.MessageEntry.COLUMN_NAME_CREATED));
            messages.add(new Message(id, userId, message, created));
            c.moveToNext();
        }

        return messages;
    }
}
